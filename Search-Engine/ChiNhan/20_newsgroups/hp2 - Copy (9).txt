"What are these?" Harry asked Ron, holding up a pack of Chocolate Frogs.
"They're not really frogs, are they?" He was starting to feel that
nothing would surprise him.
"No," said Ron. "But see what the card is. I'm missing Agrippa."
"What?"
"Oh, of course, you wouldn't know -- Chocolate Frogs have cards, inside
them, you know, to collect -- famous witches and wizards. I've got about
five hundred, but I haven't got Agrippa or Ptolemy."
Harry unwrapped his Chocolate Frog and picked up the card. It showed a
man's face. He wore half- moon glasses, had a long, crooked nose, and
flowing silver hair, beard, and mustache. Underneath the picture was the
name Albus Dumbledore.
"So this is Dumbledore!" said Harry.
"Don't tell me you'd never heard of Dumbledore!" said Ron. "Can I have a
frog? I might get Agrippa -- thanks
Harry turned over his card and read:
ALBUS DUMBLEDORE
CURRENTLY HEADMASTER OF HOGWARTS
Considered by many the greatest wizard of modern times, Dumbledore is
particularly famous for his defeat of the dark wizard Grindelwald in
1945, for the discovery of the twelve uses of dragon's blood, and his
work on alchemy with his partner, Nicolas Flamel. Professor Dumbledore
enjoys chamber music and tenpin bowling.
Harry turned the card back over and saw, to his astonishment, that
Dumbledore's face had disappeared.
"He's gone!"
"Well, you can't expect him to hang around all day," said Ron. "He'll be
back. No, I've got Morgana again and I've got about six of her... do you
want it? You can start collecting."
82
Ron's eyes strayed to the pile of Chocolate Frogs waiting to be
unwrapped.
"Help yourself," said Harry. "But in, you know, the Muggle world, people
just stay put in photos."
"Do they? What, they don't move at all?" Ron sounded amazed. "weird!"
Harry stared as Dumbledore sidled back into the picture on his card and
gave him a small smile. Ron was more interested in eating the frogs than
looking at the Famous Witches and Wizards cards, but Harry couldn't keep
his eyes off them. Soon he had not only Dumbledore and Morgana, but
Hengist of Woodcroft, Alberic Grunnion, Circe, Paracelsus, and Merlin.
He finally tore his eyes away from the druidess Cliodna, who was
scratching her nose, to open a bag of Bertie Bott's Every Flavor Beans.
"You want to be careful with those," Ron warned Harry. "When they say
every flavor, they mean every flavor -- you know, you get all the
ordinary ones like chocolate and peppermint and mar- malade, but then
you can get spinach and liver and tripe. George reckons he had a boogerflavored one once."
Ron picked up a green bean, looked at it carefully, and bit into a
corner.
"Bleaaargh -- see? Sprouts."
They had a good time eating the Every Flavor Beans. Harry got toast,
coconut, baked bean, strawberry, curry, grass, coffee, sardine, and was
even brave enough to nibble the end off a funny gray one Ron wouldn't
touch, which turned out to be pepper.
The countryside now flying past the window was becoming wilder. The neat
fields had gone. Now there were woods, twisting rivers, and dark green
hills.
There was a knock on the door of their compartment and the round-faced
boy Harry had passed on platform nine and threequarters came in. He
looked tearful.
"Sorry," he said, "but have you seen a toad at all?"
When they shook their heads, he wailed, "I've lost him! He keeps getting
83
away from me!"
"He'll turn up," said Harry.
"Yes," said the boy miserably. "Well, if you see him..."
He left.
"Don't know why he's so bothered," said Ron. "If I'd brought a toad I'd
lose it as quick as I could. Mind you, I brought Scabbers, so I can't
talk."
The rat was still snoozing on Ron's lap.
"He might have died and you wouldn't know the difference," said Ron in
disgust. "I tried to turn him yellow yesterday to make him more
interesting, but the spell didn't work. I'll show you, look..."
He rummaged around in his trunk and pulled out a very battered-looking
wand. It was chipped in places and something white was glinting at the
end.
"Unicorn hair's nearly poking out. Anyway
He had just raised his 'wand when the compartment door slid open again.
The toadless boy was back, but this time he had a girl with him. She was
already wearing her new Hogwarts robes.
"Has anyone seen a toad? Neville's lost one," she said. She had a bossy
sort of voice, lots of bushy brown hair, and rather large front teeth.
"We've already told him we haven't seen it," said Ron, but the girl
wasn't listening, she was looking at the wand in his hand.
"Oh, are you doing magic? Let's see it, then."
She sat down. Ron looked taken aback.
"Er -- all right."
He cleared his throat.
"Sunshine, daisies, butter mellow, Turn this stupid, fat rat yellow."
84
He waved his wand, but nothing happened. Scabbers stayed gray and fast
asleep.
"Are you sure that's a real spell?" said the girl. "Well, it's not very
good, is it? I've tried a few simple spells just for practice and it's
all worked for me. Nobody in my family's magic at all, it was ever such
a surprise when I got my letter, but I was ever so pleased, of course, I
mean, it's the very best school of witchcraft there is, I've heard --
I've learned all our course books by heart, of course, I just hope it
will be enough -- I'm Hermione Granger, by the way, who are you.
She said all this very fast.
Harry looked at Ron, and was relieved to see by his stunned face that he
hadn't learned all the course books by heart either.
"I'm Ron Weasley," Ron muttered.
"Harry Potter," said Harry.
"Are you really?" said Hermione. "I know all about you, of course -- I
got a few extra books. for background reading, and you're in Modern
Magical History and The Rise and Fall of the Dark Arts and Great
Wizarding Events of the Twentieth Century.
"Am I?" said Harry, feeling dazed.
"Goodness, didn't you know, I'd have found out everything I could if it
was me," said Hermione. "Do either of you know what house you'll be in?
I've been asking around, and I hope I'm in Gryffindor, it sounds by far
the best; I hear Dumbledore himself was in it, but I suppose Ravenclaw
wouldn't be too bad.... Anyway, we'd better go and look for Neville's
toad. You two had better change, you know, I expect we'll be there
soon."
And she left, taking the toadless boy with her.
"Whatever house I'm in, I hope she's not in it," said Ron. He threw his
wand back into his trunk. "Stupid spell -- George gave it to me, bet he
knew it was a dud."
"What house are your brothers in?" asked Harry.
85
"Gryffindor," said Ron. Gloom seemed to be settling on him again. "Mom
and Dad were in it, too. I don't know what they'll say if I'm not. I
don't suppose Ravenclaw would be too bad, but imagine if they put me in
Slytherin."
"That's the house Vol-, I mean, You-Know-Who was in?"
"Yeah," said Ron. He flopped back into his seat, looking depressed.
"You know, I think the ends of Scabbers' whiskers are a bit lighter,"
said Harry, trying to take Ron's mind off houses. "So what do your
oldest brothers do now that they've left, anyway?"
Harry was wondering what a wizard did once he'd finished school.
"Charlie's in Romania studying dragons, and Bill's in Africa doing
something for Gringotts," said Ron. "Did you hear about
Gringotts? It's been all over the Daily Prophet, but I don't suppose you
get that with the Muggles -- someone tried to rob a high security
vault."
Harry stared.
"Really? What happened to them?"
"Nothing, that's why it's such big news. They haven't been caught. My
dad says it must've been a powerful Dark wizard to get round Gringotts,
but they don't think they took anything, that's what's odd. 'Course,
everyone gets scared when something like this happens in case
You-Know-Who's behind it."
Harry turned this news over in his mind. He was starting to get a
prickle of fear every time You- Know-Who was mentioned. He supposed this
was all part of entering the magical world, but it had been a lot more
comfortable saying "Voldemort" without worrying.
"What's your Quidditch team?" Ron asked.
"Er -- I don't know any," Harry confessed.
"What!" Ron looked dumbfounded. "Oh, you wait, it's the best game in the
86
world --" And he was off, explaining all about the four balls and the
positions of the seven players, describing famous games he'd been to
with his brothers and the broomstick he'd like to get if he had the
money. He was just taking Harry through the finer points of the game
when the compartment door slid open yet again, but it wasn't Neville the
toadless boy, or Hermione Granger this time.
Three boys entered, and Harry recognized the middle one at once: it was
the pale boy from Madam Malkin's robe shop. He was looking at Harry with
a lot more interest than he'd shown back in Diagon Alley.
"Is it true?" he said. "They're saying all down the train that Harry
Potter's in this compartment. So it's you, is it?"
"Yes," said Harry. He was looking at the other boys. Both of them were
thickset and looked extremely mean. Standing on either side of the pale
boy, they looked like bodyguards.
"Oh, this is Crabbe and this is Goyle," said the pale boy carelessly,
noticing where Harry was looking. "And my name's Malfoy, Draco Malfoy."
Ron gave a slight cough, which might have been hiding a snigget. Draco
Malfoy looked at him.
"Think my name's funny, do you? No need to ask who you are. My father
told me all the Weasleys have red hair, freckles, and more children than
they can afford."
He turned back to Harry. "You'll soon find out some wizarding families
are much better than others, Potter. You don't want to go making friends
with the wrong sort. I can help you there."
He held out his hand to shake Harry's, but Harry didn't take it.
"I think I can tell who the wrong sort are for myself, thanks," he said
coolly.
Draco Malfoy didn't go red, but a pink tinge appeared in his pale
cheeks.
"I'd be careful if I were you, Potter," he said slowly. "Unless you're a
bit politer you'll go the same way as your parents. They didn't know
what was good for them, either. You hang around with riffraff like the
87
Weasleys and that Hagrid, and it'll rub off on you."
Both Harry and Ron stood up.
"Say that again," Ron said, his face as red as his hair.
"Oh, you're going to fight us, are you?" Malfoy sneered.
"Unless you get out now," said Harry, more bravely than he felt, because
Crabbe and Goyle were a lot bigger than him or Ron.
"But we don't feet like leaving, do we, boys? We've eaten all our food
and you still seem to have some."
Goyle reached toward the Chocolate Frogs next to Ron - Ron leapt
forward, but before he'd so much as touched Goyle, Goyle let out a
horrible yell.
Scabbers the rat was hanging off his finger, sharp little teeth sunk
deep into Goyle's knuckle - Crabbe and Malfoy backed away as Goyle swung
Scabbers round and round, howling, and when Scabbets finally flew off
and hit the window, all three of them disappeared at once. Perhaps they
thought there were more rats lurking among the sweets, or perhaps they'd
heard footsteps, because a second later, Hermione Granger had come in.
"What has been going on?" she said, looking at the sweets all over the
floor and Ron picking up Scabbers by his tail.
I think he's been knocked out," Ron said to Harry. He looked closer at
Scabbers. "No -- I don't believe it -- he's gone back to sleep-"
And so he had.
"You've met Malfoy before?"
Harry explained about their meeting in Diagon Alley.
"I've heard of his family," said Ron darkly. "They were some of the
first to come back to our side after You-Know-Who disappeared. Said
they'd been bewitched. My dad doesn't believe it. He says Malfoy's
father didn't need an excuse to go over to the Dark Side." He turned to
Hermione. "Can we help you with something?"
88
"You'd better hurry up and put your robes on, I've just been up to the
front to ask the conductor, and he says we're nearly there. You haven't
been fighting, have you? You'll be in trouble before we even get there!"
"Scabbers has been fighting, not us," said Ron, scowling at her. "Would
you mind leaving while we change?"
"All right -- I only came in here because people outside are behaving
very childishly, racing up and down the corridors," said Hermione in a
sniffy voice. "And you've got dirt on your nose, by the way, did you
know?"
Ron glared at her as she left. Harry peered out of the window. It was
getting dark. He could see mountains and forests under a deep purple
sky. The train did seem to be slowing down.
He and Ron took off their jackets and pulled on their long black robes.
Ron's were a bit short for him, you could see his sneakers underneath
them.
A voice echoed through the train: "We will be reaching Hogwarts in five
minutes' time. Please leave your luggage on the train, it will be taken
to the school separately."
Harry's stomach lurched with nerves and Ron, he saw, looked pale under
his freckles. They crammed their pockets with the last of the sweets and
joined the crowd thronging the corridor.
The train slowed right down and finally stopped. People pushed their way
toward the door and out on to a tiny, dark platform. Harry shivered in
the cold night air. Then a lamp came bobbing over the heads of the
students, and Harry heard a familiar voice: "Firs' years! Firs' years
over here! All right there, Harry?"
Hagrid's big hairy face beamed over the sea of heads.
"C'mon, follow me -- any more firs' years? Mind yer step, now! Firs'
years follow me!"
Slipping and stumbling, they followed Hagrid down what seemed to be a
steep, narrow path. It was so dark on either side of them that Harry
thought there must be thick trees there. Nobody spoke much. Neville, the
boy who kept losing his toad, sniffed once or twice.
89
"Ye' all get yer firs' sight o' Hogwarts in a sec," Hagrid called over
his shoulder, "jus' round this bend here."
There was a loud "Oooooh!"
The narrow path had opened suddenly onto the edge of a great black take.
Perched atop a high mountain on the other side, its windows sparkling in
the starry sky, was a vast castle with many turrets and towers.
"No more'n four to a boat!" Hagrid called, pointing to a fleet of little
boats sitting in the water by the shore. Harry and Ron were followed
into their boat by Neville and Hermione. "Everyone in?" shouted Hagrid,
who had a boat to himself. "Right then -- FORWARD!"
And the fleet of little boats moved off all at once, gliding across the
lake, which was as smooth as glass. Everyone was silent, staring up at
the great castle overhead. It towered over them as they sailed nearer
and nearer to the cliff on which it stood.
"Heads down!" yelled Hagrid as the first boats reached the cliff; they
all bent their heads and the little boats carried them through a curtain
of ivy that hid a wide opening in the cliff face. They were carried
along a dark tunnel, which seemed to be taking them right underneath the
castle, until they reached a kind of underground harbor, where they
clambered out onto rocks and pebbles.
"Oy, you there! Is this your toad?" said Hagrid, who was checking the
boats as people climbed out of them.
"Trevor!" cried Neville blissfully, holding out his hands. Then they
clambered up a passageway in the rock after Hagrid's lamp, coming out at
last onto smooth, damp grass right in the shadow of the castle.
They walked up a flight of stone steps and crowded around the huge, Oak
front door.
"Everyone here? You there, still got yer toad?"
Hagrid raised a gigantic fist and knocked three times on the castle
door.
90
CHAPTER SEVEN
THE SORTING HAT
The door swung open at once. A tall, black-haired witch in emerald-green
robes stood there. She had a very stern face and Harry's first thought
was that this was not someone to cross.
"The firs' years, Professor McGonagall," said Hagrid.
"Thank you, Hagrid. I will take them from here."
She pulled the door wide. The entrance hall was so big you could have
fit the whole of the Dursleys' house in it. The stone walls were lit
with flaming torches like the ones at Gringotts, the ceiling was too
high to make out, and a magnificent marble staircase facing them led to
the upper floors.
They followed Professor McGonagall across the flagged stone floor. Harry
could hear the drone of hundreds of voices from a doorway to the right
-the rest of the school must already be here -- but Professor McGonagall
showed the first years into a small, empty chamber off the hall. They
crowded in, standing rather closer together than they would usually have
done, peering about nervously.
"Welcome to Hogwarts," said Professor McGonagall. "The start-of-term
banquet will begin shortly, but before you take your seats in the Great
Hall, you will be sorted into your houses. The Sorting is a very
important ceremony because, while you are here, your house will be
something like your family within Hogwarts. You will have classes with
the rest of your house, sleep in your house dormitory, and spend free
time in your house common room.
"The four houses are called Gryffindor, Hufflepuff, Ravenclaw, and
Slytherin. Each house has its own noble history and each has produced
outstanding witches and wizards. While you are at Hogwarts, your
triumphs will earn your house points, while any rulebreaking will lose
house points. At the end of the year, the house with the most points is
awarded the house cup, a great honor. I hope each of you will be a
credit to whichever house becomes yours.
"The Sorting Ceremony will take place in a few minutes in front of the
rest of the school. I suggest you all smarten yourselves up as much as
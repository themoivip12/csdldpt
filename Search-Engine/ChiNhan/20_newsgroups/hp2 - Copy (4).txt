could you not be, my dratted sister being what she was? Oh, she got a
letter just like that and disappeared off to that-that school-and came
home every vacation with her pockets full of frog spawn, turning teacups
into rats. I was the only one who saw her for what she was -- a freak!
But for my mother and father, oh no, it was Lily this and Lily that,
they were proud of having a witch in the family!"
She stopped to draw a deep breath and then went ranting on. It seemed
she had been wanting to say all this for years.
"Then she met that Potter at school and they left and got married and
had you, and of course I knew you'd be just the same, just as strange,
just as -- as -- abnormal -- and then, if you please, she went and got
herself blown up and we got landed with you!"
Harry had gone very white. As soon as he found his voice he said, "Blown
up? You told me they died in a car crash!"
"CAR CRASH!" roared Hagrid, jumping up so angrily that the Dursleys
scuttled back to their corner. "How could a car crash kill Lily an'
James Potter? It's an outrage! A scandal! Harry Potter not knowin' his
own story when every kid in our world knows his name!" "But why? What
happened?" Harry asked urgently.
The anger faded from Hagrid's face. He looked suddenly anxious.
"I never expected this," he said, in a low, worried voice. "I had no
idea, when Dumbledore told me there might be trouble gettin' hold of
yeh, how much yeh didn't know. Ah, Harry, I don' know if I'm the right
person ter tell yeh -- but someone 3 s gotta -- yeh can't go off ter
Hogwarts not knowin'."
He threw a dirty look at the Dursleys.
"Well, it's best yeh know as much as I can tell yeh -- mind, I can't
tell yeh everythin', it's a great myst'ry, parts of it...."
He sat down, stared into the fire for a few seconds, and then said, "It
begins, I suppose, with -- with a person called -- but it's incredible
yeh don't know his name, everyone in our world knows --"
"Who? "
42
"Well -- I don' like sayin' the name if I can help it. No one does."
"Why not?"
"Gulpin' gargoyles, Harry, people are still scared. Blimey, this is
difficult. See, there was this wizard who went... bad. As bad as you
could go. Worse. Worse than worse. His name was..."
Hagrid gulped, but no words came out.
"Could you write it down?" Harry suggested.
"Nah -can't spell it. All right -- Voldemort. " Hagrid shuddered. "Don'
make me say it again. Anyway, this -- this wizard, about twenty years
ago now, started lookin' fer followers. Got 'em, too -- some were
afraid, some just wanted a bit o' his power, 'cause he was gettin'
himself power, all right. Dark days, Harry. Didn't know who ter trust,
didn't dare get friendly with strange wizards or witches... terrible
things happened. He was takin' over. 'Course, some stood up to him --
an' he killed 'em. Horribly. One o' the only safe places left was
Hogwarts. Reckon Dumbledore's the only one You-Know-Who was afraid of.
Didn't dare try takin' the school, not jus' then, anyway.
"Now, yer mum an' dad were as good a witch an' wizard as I ever knew.
Head boy an' girl at Hogwarts in their day! Suppose the myst'ry is why
You-Know-Who never tried to get 'em on his side before... probably knew
they were too close ter Dumbledore ter want anythin' ter do with the
Dark Side.
"Maybe he thought he could persuade 'em... maybe he just wanted 'em
outta the way. All anyone knows is, he turned up in the village where
you was all living, on Halloween ten years ago. You was just a year old.
He came ter yer house an' -- an' --"
Hagrid suddenly pulled out a very dirty, spotted handkerchief and blew
his nose with a sound like a foghorn.
"Sorry," he said. "But it's that sad -- knew yer mum an' dad, an' nicer
people yeh couldn't find -- anyway..."
"You-Know-Who killed 'em. An' then -- an' this is the real myst'ry of
the thing -- he tried to kill you, too. Wanted ter make a clean job of
it, I suppose, or maybe he just liked killin' by then. But he couldn't
43
do it. Never wondered how you got that mark on yer forehead? That was no
ordinary cut. That's what yeh get when a Powerful, evil curse touches
yeh -- took care of yer mum an' dad an' yer house, even -- but it didn't
work on you, an' that's why yer famous, Harry. No one ever lived after
he decided ter kill 'em, no one except you, an' he'd killed some o' the
best witches an' wizards of the age -- the McKinnons, the Bones, the
Prewetts -- an' you was only a baby, an' you lived."
Something very painful was going on in Harry's mind. As Hagrid's story
came to a close, he saw again the blinding flash of green light, more
clearly than he had ever remembered it before -- and he remembered
something else, for the first time in his life: a high, cold, cruel
laugh.
Hagrid was watching him sadly.
"Took yeh from the ruined house myself, on Dumbledore's orders. Brought
yeh ter this lot..."
"Load of old tosh," said Uncle Vernon. Harry jumped; he had almost
forgotten that the Dursleys were there. Uncle Vernon certainly seemed to
have got back his courage. He was glaring at Hagrid and his fists were
clenched.
"Now, you listen here, boy," he snarled, "I accept there's something
strange about you, probably nothing a good beating wouldn't have cured
-- and as for all this about your parents, well, they were weirdos, no
denying it, and the world's better off without them in my opinion --
asked for all they got, getting mixed up with these wizarding types --
just what I expected, always knew they'd come to a sticky end --"
But at that moment, Hagrid leapt from the sofa and drew a battered pink
umbrella from inside his coat. Pointing this at Uncle Vernon like a
sword, he said, "I'm warning you, Dursley -I'm warning you -- one more
word... "
In danger of being speared on the end of an umbrella by a bearded giant,
Uncle Vernon's courage failed again; he flattened himself against the
wall and fell silent.
"That's better," said Hagrid, breathing heavily and sitting back down on
the sofa, which this time sagged right down to the floor.
44
Harry, meanwhile, still had questions to ask, hundreds of them.
"But what happened to Vol--, sorry -- I mean, You-Know-Who?"
"Good question, Harry. Disappeared. Vanished. Same night he tried ter
kill you. Makes yeh even more famous. That's the biggest myst'ry, see...
he was gettin' more an' more powerful -- why'd he go?
"Some say he died. Codswallop, in my opinion. Dunno if he had enough
human left in him to die. Some say he's still out there, bidin' his
time, like, but I don' believe it. People who was on his side came back
ter ours. Some of 'em came outta kinda trances. Don~ reckon they
could've done if he was comin' back.
"Most of us reckon he's still out there somewhere but lost his powers.
Too weak to carry on. 'Cause somethin' about you finished him, Harry.
There was somethin' goin' on that night he hadn't counted on -- I dunno
what it was, no one does -- but somethin' about you stumped him, all
right."
Hagrid looked at Harry with warmth and respect blazing in his eyes, but
Harry, instead of feeling pleased and proud, felt quite sure there had
been a horrible mistake. A wizard? Him? How could he possibly be? He'd
spent his life being clouted by Dudley, and bullied by Aunt Petunia and
Uncle Vernon; if he was really a wizard, why hadn't they been turned
into warty toads every time they'd tried to lock him in his cupboard? If
he'd once defeated the greatest sorcerer in the world, how come Dudley
had always been able to kick him around like a football?
"Hagrid," he said quietly, "I think you must have made a mistake. I
don't think I can be a wizard."
To his surprise, Hagrid chuckled.
"Not a wizard, eh? Never made things happen when you was scared or
angry?"
Harry looked into the fire. Now he came to think about it... every odd
thing that had ever made his aunt and uncle furious with him had
happened when he, Harry, had been upset or angry... chased by Dudley's
gang, he had somehow found himself out of their reach... dreading going
to school with that ridiculous haircut, he'd managed to make it grow
back... and the very last time Dudley had hit him, hadn't he got his
45
revenge, without even realizing he was doing it? Hadn't he set a boa
constrictor on him?
Harry looked back at Hagrid, smiling, and saw that Hagrid was positively
beaming at him.
"See?" said Hagrid. "Harry Potter, not a wizard -- you wait, you'll be
right famous at Hogwarts."
But Uncle Vernon wasn't going to give in without a fight.
"Haven't I told you he's not going?" he hissed. "He's going to Stonewall
High and he'll be grateful for it. I've read those letters and he needs
all sorts of rubbish -- spell books and wands and --"
"If he wants ter go, a great Muggle like you won't stop him," growled
Hagrid. "Stop Lily an' James Potter' s son goin' ter Hogwarts! Yer mad.
His name's been down ever since he was born. He's off ter the finest
school of witchcraft and wizardry in the world. Seven years there and he
won't know himself. He'll be with youngsters of his own sort, fer a
change, an' he'll be under the greatest headmaster Hogwarts ever had
Albus Dumbled--"
"I AM NOT PAYING FOR SOME CRACKPOT OLD FOOL To TEACH HIM
MAGIC TRICKS!"
yelled Uncle Vernon.
But he had finally gone too far. Hagrid seized his umbrella and whirled
it over his head, "NEVER," he thundered, "- INSULT- ALBUS- DUMBLEDOREIN- FRONT- OF- ME!"
He brought the umbrella swishing down through the air to point at Dudley
-- there was a flash of violet light, a sound like a firecracker, a
sharp squeal, and the next second, Dudley was dancing on the spot with
his hands clasped over his fat bottom, howling in pain. When he turned
his back on them, Harry saw a curly pig's tail poking through a hole in
his trousers.
Uncle Vernon roared. Pulling Aunt Petunia and Dudley into the other
room, he cast one last terrified look at Hagrid and slammed the door
behind them.
Hagrid looked down at his umbrella and stroked his beard.
46
"Shouldn'ta lost me temper," he said ruefully, "but it didn't work
anyway. Meant ter turn him into a pig, but I suppose he was so much like
a pig anyway there wasn't much left ter do."
He cast a sideways look at Harry under his bushy eyebrows.
"Be grateful if yeh didn't mention that ter anyone at Hogwarts," he
said. "I'm -- er -- not supposed ter do magic, strictly speakin'. I was
allowed ter do a bit ter follow yeh an' get yer letters to yeh an' stuff
-- one o' the reasons I was so keen ter take on the job
"Why aren't you supposed to do magic?" asked Harry.
"Oh, well -- I was at Hogwarts meself but I -- er -- got expelled, ter
tell yeh the truth. In me third year. They snapped me wand in half an'
everything. But Dumbledore let me stay on as gamekeeper. Great man,
Dumbledore." "Why were you expelled?"
"It's gettin' late and we've got lots ter do tomorrow," said Hagrid
loudly. "Gotta get up ter town, get all yer books an' that."
He took off his thick black coat and threw it to Harry.
"You can kip under that," he said. "Don' mind if it wriggles a bit, I
think I still got a couple o' dormice in one o' the pockets."
CHAPTER FIVE
DIAGON ALLEY
Harry woke early the next morning. Although he could tell it was
daylight, he kept his eyes shut tight.
"It was a dream, he told himself firmly. "I dreamed a giant called
Hagrid came to tell me I was going to a school for wizards. When I open
my eyes I'll be at home in my cupboard."
There was suddenly a loud tapping noise.
And there's Aunt Petunia knocking on the door, Harry thought, his heart
sinking. But he still didn't open his eyes. It had been such a good
47
dream.
Tap. Tap. Tap.
"All right," Harry mumbled, "I'm getting up."
He sat up and Hagrid's heavy coat fell off him. The hut was full of
sunlight, the storm was over, Hagrid himself was asleep on the collapsed
sofa, and there was an owl rapping its claw on the window, a newspaper
held in its beak.
Harry scrambled to his feet, so happy he felt as though a large balloon
was swelling inside him. He went straight to the window and jerked it
open. The owl swooped in and dropped the newspaper on top of Hagrid, who
didn't wake up. The owl then fluttered onto the floor and began to
attack Hagrid's coat.
"Don't do that."
Harry tried to wave the owl out of the way, but it snapped its beak
fiercely at him and carried on savaging the coat.
"Hagrid!" said Harry loudly. "There's an owl
"Pay him," Hagrid grunted into the sofa.
"What?"
"He wants payin' fer deliverin' the paper. Look in the pockets."
Hagrid's coat seemed to be made of nothing but pockets -- bunches of
keys, slug pellets, balls of string, peppermint humbugs, teabags...
finally, Harry pulled out a handful of strange-looking coins.
"Give him five Knuts," said Hagrid sleepily.
"Knuts?"
"The little bronze ones."
Harry counted out five little bronze coins, and the owl held out his leg
so Harry could put the money into a small leather pouch tied to it. Then
he flew off through the open window.
48
Hagrid yawned loudly, sat up, and stretched.
"Best be Off, Harry, lots ter do today, gotta get up ter London an' buy
all yer stuff fer school."
Harry was turning over the wizard coins and looking at them. He had just
thought of something that made him feel as though the happy balloon
inside him had got a puncture.
"Um -- Hagrid?"
"Mm?" said Hagrid, who was pulling on his huge boots.
"I haven't got any money -- and you heard Uncle Vernon last night ... he
won't pay for me to go and learn magic."
"Don't worry about that," said Hagrid, standing up and scratching his
head. "D'yeh think yer parents didn't leave yeh anything?"
"But if their house was destroyed --"
"They didn' keep their gold in the house, boy! Nah, first stop fer us is
Gringotts. Wizards' bank. Have a sausage, they're not bad cold -- an' I
wouldn' say no teh a bit o' yer birthday cake, neither."
"Wizards have banks?"
"Just the one. Gringotts. Run by goblins."
Harry dropped the bit of sausage he was holding.
"Goblins?"
"Yeah -- so yeh'd be mad ter try an' rob it, I'll tell yeh that. Never
mess with goblins, Harry. Gringotts is the safest place in the world fer
anything yeh want ter keep safe -- 'cept maybe Hogwarts. As a matter o'
fact, I gotta visit Gringotts anyway. Fer Dumbledore. Hogwarts
business." Hagrid drew himself up proudly. "He usually gets me ter do
important stuff fer him. Fetchin' you gettin' things from Gringotts --
knows he can trust me, see.
"Got everythin'? Come on, then."
49
Harry followed Hagrid out onto the rock. The sky was quite clear now and
the sea gleamed in the sunlight. The boat Uncle Vernon had hired was
still there, with a lot of water in the bottom after the storm.
"How did you get here?" Harry asked, looking around for another boat.
"Flew," said Hagrid.
"Flew?"
"Yeah -- but we'll go back in this. Not s'pposed ter use magic now I've
got yeh."
They settled down in the boat, Harry still staring at Hagrid, trying to
imagine him flying.
"Seems a shame ter row, though," said Hagrid, giving Harry another of
his sideways looks. "If I was ter -- er -- speed things up a bit, would
yeh mind not mentionin' it at Hogwarts?"
"Of course not," said Harry, eager to see more magic. Hagrid pulled out
the pink umbrella again, tapped it twice on the side of the boat, and
they sped off toward land.
"Why would you be mad to try and rob Gringotts?" Harry asked.
"Spells -- enchantments," said Hagrid, unfolding his newspaper as he
spoke. "They say there's dragons guardin' the highsecurity vaults. And
then yeh gotta find yer way -- Gringotts is hundreds of miles under
London, see. Deep under the Underground. Yeh'd die of hunger tryin' ter
get out, even if yeh did manage ter get yer hands on summat."
Harry sat and thought about this while Hagrid read his newspaper, the
Daily Prophet. Harry had learned from Uncle Vernon that people liked to
be left alone while they did this, but it was very difficult, he'd never
had so many questions in his life.
"Ministry o' Magic messin' things up as usual," Hagrid muttered, turning
the page.
"There's a Ministry of Magic?" Harry asked, before he could stop
himself.
"'Course," said Hagrid. "They wanted Dumbledore fer Minister, 0 '
50
course, but he'd never leave Hogwarts, so old Cornelius Fudge got the
job. Bungler if ever there was one. So he pelts Dumbledore with owls
every morning, askin' fer advice."
"But what does a Ministry of Magic do?"
"Well, their main job is to keep it from the Muggles that there's still
witches an' wizards up an' down the country."
"Why?"
"Why? Blimey, Harry, everyone'd be wantin' magic solutions to their
problems. Nah, we're best left alone."
At this moment the boat bumped gently into the harbor wall. Hagrid
folded up his newspaper, and they clambered up the stone steps onto the
street.
Passersby stared a lot at Hagrid as they walked through the little town
to the station. Harry couldn't blame them. Not only was Hagrid twice as
tall as anyone else, he kept pointing at perfectly ordinary things like
parking meters and saying loudly, "See that, Harry? Things these Muggles
dream up, eh?"
"Hagrid," said Harry, panting a bit as he ran to keep up, "did you say
there are dragons at Gringotts?"
"Well, so they say," said Hagrid. "Crikey, I'd like a dragon."
"You'd like one?"
"Wanted one ever since I was a kid -- here we go."
They had reached the station. There was a train to London in five
minutes' time. Hagrid, who didn't understand "Muggle money," as he
called it, gave the bills to Harry so he could buy their tickets.
People stared more than ever on the train. Hagrid took up two seats and
sat knitting what looked like a canary-yellow circus tent.
"Still got yer letter, Harry?" he asked as he counted stitches. Harry
took the parchment envelope out of his pocket.